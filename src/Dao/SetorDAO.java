package Dao;

import Conexao.ConectaBd;
import Dominio.Setor;
import Dominio.Solicitante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SetorDAO {
    private static final String SELECT = "SELECT * FROM public.setor";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.setor WHERE cod_setor = ?";
    private static final String INSERT = "INSERT INTO public.setor(" +
                                        "cod_setor, nome_setor, cod_solicitante,cod_responsavel)" +
                                        "VALUES (?, ?, ?,?)";
    private static final String DELETE = "DELETE FROM public.setor" +
                                        "WHERE cod_setor = ?";
   private static final String SELECT_SOLICITANTE = "SELECT * FROM public.setor WHERE cod_solicitante = ?";
   private static final String UPDATE_NOME = "UPDATE public.setor SET nome_setor = ? WHERE cod_setor = ?";
  
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    private IndividualDAO ind = new IndividualDAO();
    private SolicitanteDAO sol = new SolicitanteDAO();
    private Solicitante solicitante;
    
    public int salvarSetor(Setor setor) throws SQLException{
        preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, setor.getCodigo());
        preparedStatement.setString(2, setor.getNome());
        int codSolicitante = sol.salvarSolicitante(setor.getEmail());
        preparedStatement.setInt(3, codSolicitante);
        preparedStatement.setInt(4, setor.getResponsavel().getCodigo());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        rs.next();
        return rs.getInt(1);
    }
    public List<Setor> selecionarSetor() throws SQLException{
       List<Setor> setores = new ArrayList<Setor>();
       Setor setor;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           setor = new Setor(rs.getInt("cod_setor"),
                             rs.getString("nome_setor"),
                             ind.buscarIndividual(rs.getInt("cod_responsavel")));
           solicitante = sol.buscarSolicitante(rs.getInt("cod_solicitante"));
           setor.setCodigoSolicitante(solicitante.getCod_solicitante());
           setor.setEmail(solicitante.getEmail());
           setor.setCodigo(rs.getInt("cod_setor"));
           setores.add(setor);
       }
       return setores;
    }
    public Setor buscarSetor(int cod) throws SQLException {
        Setor setor;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
        setor = new Setor(rs.getInt("cod_setor"),
                             rs.getString("nome_setor"),
                             ind.buscarIndividual(rs.getInt("cod_responsavel")));
           solicitante = sol.buscarSolicitante(rs.getInt("cod_solicitante"));
           setor.setCodigoSolicitante(solicitante.getCod_solicitante());
           setor.setEmail(solicitante.getEmail());
           setor.setCodigo(rs.getInt("cod_setor"));
        return setor;
        }
        return null;
    }
    public void deletarSetor(Setor setor) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, setor.getCodigo());
       preparedStatement.execute();
    }
    
    public boolean mudaNome(Setor setor) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_NOME);
        preparedStatement.setString(1, setor.getNome());
        preparedStatement.setInt(2,setor.getCodigo());
        preparedStatement.execute();
        sol.mudaEmail(setor.getCod_solicitante(), setor.getEmail());
        return true;
    }
    
    public Setor buscaBySolicitante(int cod) throws SQLException{
        Setor setor;
        preparedStatement = con.prepareStatement(SELECT_SOLICITANTE);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            setor = new Setor(rs.getInt("cod_setor"),
                             rs.getString("nome_setor"),
                             ind.buscarIndividual(rs.getInt("cod_responsavel")));
           solicitante = sol.buscarSolicitante(rs.getInt("cod_solicitante"));
           setor.setCodigoSolicitante(solicitante.getCod_solicitante());
           setor.setEmail(solicitante.getEmail());
           setor.setCodigo(rs.getInt("cod_setor"));
            return setor;
        }
        return null;
   }
}
