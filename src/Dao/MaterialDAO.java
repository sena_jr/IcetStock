package Dao;

import Conexao.ConectaBd;
import Dominio.Material;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MaterialDAO {
    private static final String SELECT = "SELECT * FROM public.material";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.material WHERE cod_material = ?";
    private static final String INSERT = "INSERT INTO public.material(" +
                                        "descricao, quantidade, cod_unidade)" +
                                        "VALUES (?, ?, ?)";
    private static final String DELETE = "DELETE FROM public.material" +
                                        "WHERE cod_material  = ?";
    private static final String UPDATE_ENTRADA = "UPDATE public.material SET quantidade = quantidade + ? WHERE cod_material =  ?";
    private static final String UPDATE_SAIDA = "UPDATE public.material SET quantidade = quantidade - ? WHERE cod_material = ?";
    private static final String UPDATE_NOME = "UPDATE public.material SET descricao = ? WHERE cod_material = ?";
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    private UnidadeDAO unidadeDAO = new UnidadeDAO();
    
    public int salvarMaterial(Material material) throws SQLException{
       preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
       preparedStatement.setString(1, material.getDescricao());
       preparedStatement.setInt(2, material.getQuantidade());
       preparedStatement.setInt(3, material.getTipo_unidade().getCod_unidade());
       preparedStatement.execute();
       ResultSet rs = preparedStatement.getGeneratedKeys();
       rs.next();
       return rs.getInt(1);
   }
    public List<Material> selecionarMaterial() throws SQLException{
       List<Material> materiais = new ArrayList<Material>();
       Material material;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           material = new Material(rs.getInt("quantidade"),
                                   rs.getString("descricao"),
                                   unidadeDAO.buscarUnidade(rs.getInt("cod_unidade")));
           material.setCodigo(rs.getInt("cod_material"));
           materiais.add(material);
       }
       return materiais;
    }
    
    public Material buscarMaterial(int cod) throws SQLException {
        Material mat = null;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            mat = new Material(rs.getInt("quantidade"),
                    rs.getString("descricao"),
                    unidadeDAO.buscarUnidade(rs.getInt("cod_unidade")));
            mat.setCodigo(rs.getInt("cod_material"));
            return mat;
        }
        return mat;
    }
    public void deletarMaterial(Material material) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, material.getCodigo());
       preparedStatement.execute();
    }

    public void entradaNoEstoque(Material material,int quantidade) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_ENTRADA);
        preparedStatement.setInt(1, quantidade);
        preparedStatement.setInt(2,material.getCodigo());
        preparedStatement.execute();
    }
    public boolean baixaNoEstoque(Material material,int quantidade) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_SAIDA);
        preparedStatement.setInt(1, quantidade);
        preparedStatement.setInt(2,material.getCodigo());
        preparedStatement.execute();
        return true;
    }
    public boolean mudaNome(Material material) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_NOME);
        preparedStatement.setString(1, material.getDescricao());
        preparedStatement.setInt(2,material.getCodigo());
        preparedStatement.execute();
        return true;
    }
}
