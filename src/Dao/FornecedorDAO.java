package Dao;

import Conexao.ConectaBd;
import Dominio.Fornecedor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FornecedorDAO {
    private static final String SELECT = "SELECT * FROM public.fornecedor";
    private static final String SELECT_ESPECIFIC  = "SELECT * FROM public.fornecedor WHERE cod_fornecedor = ?";
    private static final String INSERT = "INSERT INTO public.fornecedor(" +
                                        "cod_fornecedor, nome_fornecedor)" +
                                        "VALUES (?, ?)";
    private static final String DELETE = "DELETE FROM public.fornecedor\n" +
                                        "WHERE cod_fornecedor LIKE ?";
    private Connection con = new ConectaBd().getConexao();
    private PreparedStatement preparedStatement;
    
    public void salvarFornecedor(Fornecedor fornecedor) throws SQLException{
        preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, fornecedor.getCod_fornecedor());
        preparedStatement.setString(2,fornecedor.getNome_fornecedor());
        preparedStatement.execute();
       
    }
    public List<Fornecedor> selecionarFornecedor() throws SQLException{
        List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
        Fornecedor fornecedor;
        preparedStatement = con.prepareStatement(SELECT);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            fornecedor = new Fornecedor(rs.getInt("cod_fornecedor"),
                                        rs.getString("nome_fornecedor"));
            fornecedores.add(fornecedor);
        }
        return fornecedores;
    }
    public Fornecedor buscarFornecedor(int cod) throws SQLException{
        Fornecedor fornecedor;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1,cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            fornecedor = new Fornecedor(rs.getInt("cod_fornecedor"), rs.getString("nome_fornecedor"));
            System.out.println(rs.getInt("cod_fornecedor")+""+ rs.getString("nome_fornecedor"));
            return fornecedor;
        }
        return null;     
    }
    public void excluirFornecedor(Fornecedor fornecedor) throws SQLException{
        preparedStatement = con.prepareStatement(SELECT);
        preparedStatement.setInt(1, fornecedor.getCod_fornecedor());
        preparedStatement.execute();
    }
}
