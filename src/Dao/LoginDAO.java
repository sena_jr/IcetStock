package Dao;

import Conexao.ConectaBd;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class LoginDAO {
    private static final String acesso = "SELECT * FROM administrador where login = ? AND senha = ?";
    
    Connection con = new ConectaBd().getConexao();
    PreparedStatement preparedStatement;
    public boolean acesso(String login, String senha) throws SQLException{
        preparedStatement = con.prepareStatement(acesso);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, senha);
        ResultSet rs = preparedStatement.executeQuery();

        while(rs.next()){
            System.out.println(rs.getString("login"));
            return true;
        }

        return false;
    }
}
