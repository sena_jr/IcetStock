package Dao;

import Conexao.ConectaBd;
import Dominio.Entrada;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EntradaDAO {
    private static final String SELECT = "SELECT * FROM public.entrada";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.entrada WHERE cod_entrada = ?";
    private static final String INSERT = "INSERT INTO public.entrada(data, cod_aquisicao, cod_fornecedor)" +
                                         "VALUES (?, ?, ?)";
    private static final String DELETE = "DELETE FROM public.entrada WHERE cod_entrada LIKE ?";
   private Connection con = new ConectaBd().getConexao();
   private AquisicaoDAO aquisicao = new AquisicaoDAO();
   private FornecedorDAO fornecedor= new FornecedorDAO();
   private PreparedStatement preparedStatement;
   private Date dataSQL;
   
   public int salvarEntrada(Entrada entrada) throws SQLException{
       dataSQL = new Date(entrada.getData_entrada().getTimeInMillis());
       ItemRetiradaDAO itens = new ItemRetiradaDAO();
       preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
       preparedStatement.setDate(1, dataSQL);
       preparedStatement.setInt(2,entrada.getTipo_aquisicao().getCod_aquisicao() );
       preparedStatement.setInt(3, entrada.getFornecedor().getCod_fornecedor());
       preparedStatement.execute();
       ResultSet rs = preparedStatement.getGeneratedKeys();
       rs.next();
       return rs.getInt(1);
   }
   public List<Entrada> selecionarEntrada() throws SQLException{
       List<Entrada> entradas = new ArrayList<Entrada>();
       Entrada entrada;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           Calendar cal = Calendar.getInstance();
           
           cal.setTime(rs.getDate("data"));
           entrada = new Entrada(cal,
                   aquisicao.buscaAquisicao(rs.getInt("cod_aquisicao")),
                   fornecedor.buscarFornecedor(rs.getInt("cod_fornecedor")));
           entrada.setCodigo_entrada(rs.getInt("cod_entrada"));
           entradas.add(entrada);
       }
       return entradas;
   }
   public Entrada buscarEntrada(int cod) throws SQLException{
       Entrada entrada =null;
       preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
       preparedStatement.setInt(1, cod);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
        Calendar cal = Calendar.getInstance();
        cal.setTime(rs.getDate("data"));
        
        entrada = new Entrada(cal,
                    aquisicao.buscaAquisicao(rs.getInt("cod_aquisicao")),
                    fornecedor.buscarFornecedor(rs.getInt("cod_fornecedor")));
        entrada.setCodigo_entrada(rs.getInt("cod_entrada"));
        return entrada;
       }
       return null;
   }
   public void deletarEntrada(Entrada entrada) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, entrada.getCodigo_entrada());
       preparedStatement.execute();
   }

   public List<Entrada> buscarRetiradaHoje(Calendar data) throws SQLException, ParseException {
        List<Entrada> entradas = new ArrayList<Entrada>();
        ItemRetiradaDAO itens = new ItemRetiradaDAO();
        Calendar cal = Calendar.getInstance();
        Entrada entrada;
        dataSQL = new Date(data.getTimeInMillis());
        preparedStatement = con.prepareStatement(SELECT);
        //preparedStatement.setDate(1,dataSQL);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){    
            cal.setTime(rs.getDate("data"));
            entrada = new Entrada(cal,
                    aquisicao.buscaAquisicao(rs.getInt("cod_aquisicao")),
                    fornecedor.buscarFornecedor(rs.getInt("cod_fornecedor")));
            entrada.setCodigo_entrada(rs.getInt("cod_entrada"));
           entradas.add(entrada);
       }
       return entradas;
        
    }
}
