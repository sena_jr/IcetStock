package Dao;

import Conexao.ConectaBd;
import Dominio.Aquisicao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AquisicaoDAO {
    private static final String SELECT = "SELECT * FROM public.aquisicao";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.aquisicao WHERE cod_aquisicao = ?";
    private static final String INSERT = "INSERT INTO public.aquisicao(" +
                                         "tipo_aquisicao)" +
                                         "VALUES (?)";
    private static final String DELETE = "DELETE FROM public.aquisicao" +
                                         "WHERE cod_aquisicao LIKE ?";
    private Connection con = new ConectaBd().getConexao();
    private PreparedStatement preparedStatement;
    
    public int salvarAquisicao(Aquisicao aquisicao) throws SQLException{
        preparedStatement = con.prepareStatement(INSERT,Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, aquisicao.getAquisicao());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        rs.next();
        int cod = rs.getInt(1);
        rs.close();
        return cod;
        
    }
    public List<Aquisicao> selecionarAquisicao() throws SQLException{
        List<Aquisicao> aquisicoes = new ArrayList<>();
        Aquisicao aquisicao;
        preparedStatement = con.prepareStatement(SELECT);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            aquisicao = new Aquisicao(rs.getString("tipo_aquisicao"));
            aquisicao.setCod_aquisicao(rs.getInt("cod_aquisicao"));
            aquisicoes.add(aquisicao);
        }
        rs.close();
        return aquisicoes;
    }
    public Aquisicao buscaAquisicao(int cod) throws SQLException{
        Aquisicao aquisicao = null;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            aquisicao = new Aquisicao(rs.getString("tipo_aquisicao"));
            aquisicao.setCod_aquisicao(rs.getInt("cod_aquisicao"));
            return aquisicao;
        }
        rs.close();
        return null;
    }
    public void deletarAquisicao(Aquisicao aquisicao) throws SQLException{
        preparedStatement = con.prepareStatement(DELETE);
        preparedStatement.setInt(1,aquisicao.getCod_aquisicao());
        preparedStatement.execute();
        
    }
}
