package Dao;

import Conexao.ConectaBd;
import Dominio.Solicitante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SolicitanteDAO {
    private static final String SELECT = "SELECT * FROM public.solicitante";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.solicitante WHERE cod_solicitante = ?";
    private static final String INSERT = "INSERT INTO public.solicitante(" +
                                        "email)" +
                                        "VALUES (?)";
    private static final String DELETE = "DELETE FROM public.solicitante" +
                                        "WHERE cod_solicitante = ?"; 
    private static final String UPDATE_EMAIL = "UPDATE public.solicitante SET email = ? WHERE cod_solicitante = ?";
    
    private Connection con = new ConectaBd().getConexao();
    private PreparedStatement preparedStatement;
    
    public int salvarSolicitante(String email) throws SQLException{
        
        preparedStatement = con.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, email);
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if(rs.next())
            return rs.getInt(1);
        else
            return -1;
       
        
    }
    public List<Solicitante> selecionarSolicitante() throws SQLException{
       List<Solicitante> solicitantes = new ArrayList<Solicitante>();
       Solicitante solicitante;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           solicitante = new Solicitante();
           solicitante.setCodigoSolicitante(rs.getInt("cod_solicitante"));
           
           solicitantes.add(solicitante);
       }
       return solicitantes;
    }
    public Solicitante buscarSolicitante(int codigo) throws SQLException {
        Solicitante solicitante;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, codigo);
        ResultSet rs = preparedStatement.executeQuery();
        while( rs.next()){
            solicitante = new Solicitante();
            solicitante.setCodigoSolicitante(rs.getInt("cod_solicitante"));
            solicitante.setEmail(rs.getString("email"));
            return solicitante;
        }
        return null;
    }
    public void deletarSetor(Solicitante solicitante) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, solicitante.getCod_solicitante());
       preparedStatement.execute();
    }

     public Solicitante buscarSolicitanteChildren(int cod) throws SQLException {
        Solicitante solicitante = null;
        solicitante = new IndividualDAO().buscaBySolicitante(cod);
        if(solicitante != null){
            return solicitante;
        }else if(new SetorDAO().buscaBySolicitante(cod) != null ){
            return solicitante = new SetorDAO().buscaBySolicitante(cod);
        }
        return null;          
        
    }

     public boolean mudaEmail(int cod, String email) throws SQLException{
         preparedStatement = con.prepareStatement(UPDATE_EMAIL);
         preparedStatement.setString(1, email);
         preparedStatement.setInt(2, cod);
         preparedStatement.execute();
         return true;
     }
}
