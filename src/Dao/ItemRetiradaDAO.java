package Dao;

import Conexao.ConectaBd;
import Dominio.ItemMaterial;
import Dominio.ItemRetirada;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemRetiradaDAO {
    private final static String insert = "INSERT INTO public.item_retirada(cod_retirada,cod_barra,quantidade)" +
                                         "VALUES (?, ?, ?)";
    private final static String select_item = "SELECT * FROM public.item_retirada WHERE cod_retirada = ?";
    
    private Connection con = new ConectaBd().getConexao();
    private PreparedStatement preparedStatement;
    private ItemMaterialDAO itemMaterialDAO= new ItemMaterialDAO();
    public boolean inserir(int idRetirada,List<ItemRetirada> materiais) throws SQLException{
        preparedStatement = con.prepareStatement(insert);
        for(ItemRetirada i: materiais){
            preparedStatement.setInt(1, idRetirada);
            preparedStatement.setInt(2,i.getItemMaterial().getCodigo_barra());
            preparedStatement.setInt(3, i.getQuantidade());
            preparedStatement.execute();
            itemMaterialDAO.baixaEmEstoque(i.getItemMaterial(), i.getQuantidade());
        }
        return true;
    }
    public List<ItemRetirada> listarItens(int cod_retirada) throws SQLException{
        List<ItemRetirada> materiais = new ArrayList<>();
        ItemRetirada material;
        preparedStatement = con.prepareStatement(select_item);
        preparedStatement.setInt(1, cod_retirada);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            material = new ItemRetirada(new ItemMaterialDAO().buscarItemMaterial(rs.getInt("cod_barra")),
                    rs.getInt("quantidade"));
            materiais.add(material);
        }
        return materiais;
    }
}
