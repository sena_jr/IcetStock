package Dao;

import Conexao.ConectaBd;
import Dominio.Unidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UnidadeDAO {
    private static final String SELECT = "SELECT * FROM public.unidade";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.unidade WHERE cod_unidade = ?";
           
    private static final String INSERT = "INSERT INTO public.unidade(" +
                                        "cod_unidade, tipo_unidade)" +
                                        "VALUES (?, ?)";
    private static final String DELETE = "DELETE FROM public.unidade" +
                                         "WHERE cod_unidade = ?";
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    
    public int salvarUnidade(Unidade unidade) throws SQLException{
        preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, unidade.getCod_unidade());
        preparedStatement.setString(2, unidade.getUnidade());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        rs.next();
        return rs.getInt(1);
    }
    public List<Unidade> selecionarUnidade() throws SQLException{
       List<Unidade> unidades = new ArrayList<Unidade>();
       Unidade unidade;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           unidade = new Unidade(rs.getString("tipo_unidade"));
           unidade.setCod_Unidade(rs.getInt("cod_unidade"));
           unidades.add(unidade);
       }
       return unidades;
    }
    public Unidade buscarUnidade(int cod) throws SQLException{
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Unidade unidade = new Unidade(rs.getString("tipo_unidade"));
        unidade.setCod_Unidade(rs.getInt("cod_unidade"));
        return unidade;
    }
    public void deletarUnidade(Unidade unidade) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, unidade.getCod_unidade());
       preparedStatement.execute();
    }
}
