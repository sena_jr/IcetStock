package Dao;

import Conexao.ConectaBd;
import Dominio.Retirada;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RetiradaDAO {
    private static final String SELECT = "SELECT * FROM public.retirada" ;
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.retirada WHERE cod_retirada LIKE ?";
    private static final String SELECT_HOJE = "SELECT * FROM public.retirada WHERE  CAST(data AS text) LIKE ?";
    private static final String INSERT = "INSERT INTO public.retirada(" +
                                         " data, cod_solicitante)" +
                                         "VALUES (?, ?)";
    private static final String DELETE = "DELETE FROM public.retirada" +
                                        "WHERE cod_retirada = ?";
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    private SolicitanteDAO sol = new SolicitanteDAO();
    private java.sql.Date dataSQL;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    public int salvarRetirada(Retirada retirada) throws SQLException{
        dataSQL = new Date(retirada.getData_saida().getTimeInMillis());
        ItemRetiradaDAO itens = new ItemRetiradaDAO();
        preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setDate(1, dataSQL);
        preparedStatement.setInt(2, retirada.getSolicitante().getCod_solicitante());
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        
        rs.next();
        
        itens.inserir(rs.getInt(1), retirada.getMaterial());
        return rs.getInt(1);
    }
    public List<Retirada> selecionarRetirada() throws SQLException{
       List<Retirada> retiradas = new ArrayList<Retirada>();
       ItemRetiradaDAO itens = new ItemRetiradaDAO();
       Retirada retirada;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
            Calendar cal = Calendar.getInstance();
            cal.setTime(rs.getDate("data"));
           retirada = new Retirada(cal,
                                   sol.buscarSolicitante(rs.getInt("cod_solicitante")),
                                    itens.listarItens(rs.getInt("cod_retirada")));
           retirada.setCodigo(rs.getInt("cod_retirada"));
           retiradas.add(retirada);
       }
       return retiradas;
    }
    public Retirada buscarRetirada(int cod) throws SQLException {
        Retirada retirada;
        ItemRetiradaDAO itens = new ItemRetiradaDAO();
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Calendar cal = Calendar.getInstance();
        cal.setTime(rs.getDate("data"));
        retirada = new Retirada(cal,
                                sol.buscarSolicitante(rs.getInt("cod_solicitante")),
                                itens.listarItens(rs.getInt("cod_retirada")));
        retirada.setCodigo(rs.getInt("cod_retirada"));
        return retirada;
    }
    public List<Retirada> buscarRetiradaHoje(Calendar data) throws SQLException, ParseException {
        List<Retirada> retiradas = new ArrayList<Retirada>();
        ItemRetiradaDAO itens = new ItemRetiradaDAO();
        Calendar cal = Calendar.getInstance();
        Retirada retirada;
        dataSQL = new Date(data.getTimeInMillis());
        preparedStatement = con.prepareStatement(SELECT);
        //preparedStatement.setDate(1,dataSQL);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){    
            cal.setTime(rs.getDate("data"));
            retirada = new Retirada(cal,
                                   sol.buscarSolicitante(rs.getInt("cod_solicitante")),
                                    itens.listarItens(rs.getInt("cod_retirada")));
           retirada.setCodigo(rs.getInt("cod_retirada"));
           retiradas.add(retirada);
       }
       return retiradas;
        
    }
    public void deletarRetirada(Retirada retirada) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, retirada.getCodigo());
       preparedStatement.execute();
    }
}
