package Dao;

import Conexao.ConectaBd;
import Dominio.ItemMaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ItemMaterialDAO {
    private static final String SELECT = "SELECT * FROM public.item_material";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.item_material WHERE cod_barra = ?";
    private static final String SELECT_ESPECIFIC_BY_MATERIAL = "SELECT * FROM public.item_material WHERE cod_material= ?";
    private static final String SELECT_ESPECIFIC_BY_ENTRADA = "SELECT * FROM public.item_material WHERE cod_entrada= ?";
    private static final String INSERT = "INSERT INTO public.item_material(" +
                                        "cod_barra, cod_material, cod_entrada, quantidade)" +
                                        "VALUES (?, ?, ?,?)";
    private static final String DELETE = "DELETE FROM public.item_material WHERE cod_barra = ?";
    private static final String UPDATE_SAIDA = "UPDATE public.item_material SET quantidade = quantidade - ? WHERE cod_barra = ?";
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    private MaterialDAO materialDAO = new MaterialDAO();
    private EntradaDAO entrada = new EntradaDAO();
    
    public int salvarItemMaterial(ItemMaterial item) throws SQLException{
       preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
       preparedStatement.setInt(1, item.getCodigo_barra());
       preparedStatement.setInt(2, item.getTipoMaterial().getCodigo());
       preparedStatement.setInt(3, item.getEntrada().getCodigo_entrada());
       preparedStatement.setInt(4, item.getQuantidade());
       preparedStatement.execute();
       ResultSet rs = preparedStatement.getGeneratedKeys();
       rs.next();
       return rs.getInt(1);
   }
    public List<ItemMaterial> selecionarItemMaterial() throws SQLException{
       List<ItemMaterial> itens = new ArrayList<ItemMaterial>();
       ItemMaterial item;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           item = new ItemMaterial(rs.getInt("cod_barra"),
                   materialDAO.buscarMaterial(rs.getInt("cod_material")),
                   entrada.buscarEntrada(rs.getInt("cod_entrada")),
                   rs.getInt("quantidade"));
           item.setCodigo_barra(rs.getInt("cod_barra"));
           itens.add(item);
       }
       return itens;
   }
    public ItemMaterial buscarItemMaterial(int cod) throws SQLException{
        ItemMaterial item = null;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        if(rs.next()){
            item = new ItemMaterial(rs.getInt("cod_barra"),
                            materialDAO.buscarMaterial(rs.getInt("cod_material")),
                       entrada.buscarEntrada(rs.getInt("cod_entrada")),
                       rs.getInt("quantidade"));
            item.setCodigo_barra(rs.getInt("cod_barra"));
        }
        System.out.println(item);
        return item;
   }
    public void deletarItemMaterial(ItemMaterial item) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, item.getCodigo_barra());
       preparedStatement.execute();
       materialDAO.baixaNoEstoque(item.getTipoMaterial(), item.getQuantidade());
   }

    public List<ItemMaterial> selecionarItemMaterialByMaterial(int cod) throws SQLException{
       List<ItemMaterial> itens = new ArrayList<ItemMaterial>();
       ItemMaterial item;
       preparedStatement = con.prepareStatement(SELECT_ESPECIFIC_BY_MATERIAL);
       preparedStatement.setInt(1, cod);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           item = new ItemMaterial(rs.getInt("cod_barra"),
                   materialDAO.buscarMaterial(rs.getInt("cod_material")),
                   entrada.buscarEntrada(rs.getInt("cod_entrada")),
                   rs.getInt("quantidade"));
           item.setCodigo_barra(rs.getInt("cod_barra"));
           itens.add(item);
       }
       return itens;
    }
    public List<ItemMaterial> selecionarItemMaterialByEntrada(int cod) throws SQLException{
       List<ItemMaterial> itens = new ArrayList<ItemMaterial>();
       ItemMaterial item;
       preparedStatement = con.prepareStatement(SELECT_ESPECIFIC_BY_ENTRADA);
       preparedStatement.setInt(1, cod);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           item = new ItemMaterial(rs.getInt("cod_barra"),
                   materialDAO.buscarMaterial(rs.getInt("cod_material")),
                   entrada.buscarEntrada(rs.getInt("cod_entrada")),
                   rs.getInt("quantidade"));
           item.setCodigo_barra(rs.getInt("cod_barra"));
           itens.add(item);
       }
       return itens;
    }
    public boolean baixaEmEstoque(ItemMaterial item, int quantidade) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_SAIDA);
        preparedStatement.setInt(1, quantidade);
        preparedStatement.setInt(2,item.getCodigo_barra());
        preparedStatement.execute();
        materialDAO.baixaNoEstoque(item.getTipoMaterial(), quantidade);
        return true;
    }
}
