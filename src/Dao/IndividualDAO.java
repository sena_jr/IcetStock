package Dao;

import Conexao.ConectaBd;
import Dominio.Individual;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IndividualDAO {
    private static final String SELECT = "SELECT * FROM public.individual";
    private static final String SELECT_ESPECIFIC = "SELECT * FROM public.individual WHERE siape = ?";
    private static final String SELECT_SOLICITANTE = "SELECT * FROM public.individual WHERE cod_solicitante = ?";
    private static final String INSERT = "INSERT INTO public.individual(" +
                                        "siape, nome, cod_solicitante)" +
                                        "VALUES (?, ?, ?)";
    private static final String DELETE = "DELETE FROM public.individual" +
                                        "WHERE siape = ?";
    private static final String UPDATE_NOME = "UPDATE public.individual SET nome = ? WHERE siape = ?";
    private PreparedStatement preparedStatement;
    private Connection con = new ConectaBd().getConexao();
    private SolicitanteDAO sol = new SolicitanteDAO();
    
    public int salvarIndividual(Individual individual) throws SQLException{
       preparedStatement = con.prepareStatement(INSERT,PreparedStatement.RETURN_GENERATED_KEYS);
       int cod_solicitante = sol.salvarSolicitante(individual.getEmail());
       preparedStatement.setInt(1, individual.getCodigo());
       preparedStatement.setString(2,individual.getNome());
       preparedStatement.setInt(3, cod_solicitante);
       preparedStatement.execute();
       ResultSet rs = preparedStatement.getGeneratedKeys();
       rs.next();
       return rs.getInt(1);
   }
    public List<Individual> selecionarUsuario() throws SQLException{
       List<Individual> individuais = new ArrayList<Individual>();
       Individual individual;
       preparedStatement = con.prepareStatement(SELECT);
       ResultSet rs = preparedStatement.executeQuery();
       while(rs.next()){
           individual = new Individual(rs.getInt("SIAPE"),
                                      rs.getString("nome"));
           
           System.out.println(rs.getInt("cod_solicitante"));
           individual.setEmail((sol.buscarSolicitante(rs.getInt("cod_solicitante"))).getEmail());
           individual.setCodigoSolicitante(rs.getInt("cod_solicitante"));
           individuais.add(individual);
       }
       return individuais;
   }
 
    public Individual buscarIndividual(int cod) throws SQLException{
        Individual individual =null;
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            individual = new Individual(rs.getInt("siape"),
                           rs.getString("nome"));
            individual.setEmail((sol.buscarSolicitante(rs.getInt("cod_solicitante"))).getEmail());
            individual.setCodigoSolicitante(rs.getInt("cod_solicitante"));
            return individual;
        }
        return individual;
   }
    public List<Individual> buscarIndividualLikeSIAPE(int cod) throws SQLException{
        Individual individual;
        List<Individual> solicitantes = new ArrayList<Individual>();
        preparedStatement = con.prepareStatement(SELECT_ESPECIFIC);
        preparedStatement.setInt(1,'%'+ cod+'%');
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            individual = new Individual(rs.getInt("siape"),
                           rs.getString("nome"));
            individual.setEmail((sol.buscarSolicitante(rs.getInt("cod_solicitante"))).getEmail());
            individual.setCodigoSolicitante(rs.getInt("cod_solicitante"));
            solicitantes.add(individual);
        }
        return solicitantes;
   }
    public Individual buscaBySolicitante(int cod) throws SQLException{
        Individual individual;
        preparedStatement = con.prepareStatement(SELECT_SOLICITANTE);
        preparedStatement.setInt(1, cod);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            individual = new Individual(rs.getInt("siape"),
                           rs.getString("nome"));
            individual.setEmail((sol.buscarSolicitante(cod)).getEmail());
            individual.setCodigoSolicitante(rs.getInt("cod_solicitante"));
            return individual;
        }
        return null;
    }
    public void deletarIndividual(Individual individual) throws SQLException{
       preparedStatement = con.prepareStatement(DELETE);
       preparedStatement.setInt(1, individual.getCodigo());
       preparedStatement.execute();
    }
   
    public boolean mudaNome(Individual individual) throws SQLException{
        preparedStatement = con.prepareStatement(UPDATE_NOME);
        preparedStatement.setString(1, individual.getNome());
        preparedStatement.setInt(2,individual.getCodigo());
        preparedStatement.execute();
        sol.mudaEmail(individual.getCod_solicitante(), individual.getEmail());
        return true;
    }
}
