
package Conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConectaBd {

    private static Connection conexao;
    
    
    public void iniciarConexao() throws ClassNotFoundException{
        try{ 
           Class.forName("org.postgresql.Driver");
           //conexao = DriverManager.getConnection("jdbc:postgresql://localhost:5432/IcetStock","postgres","root");
            conexao = DriverManager.getConnection("jdbc:postgresql://localhost:5433/IcetStock","postgres","root"); //-> pc laura
            //conexao = DriverManager.getConnection("jdbc:postgresql://localhost:8080/IcetStock","postgres","root"); //-> pc Sena
            System.out.println("ok");
        }     
        catch( SQLException error ){
            JOptionPane.showMessageDialog(null, error);
        }
    }
    public Connection getConexao(){
        return this.conexao;
    }
    public void fechaConexao() throws SQLException{
        conexao.close();
    }
    
    
} 
     
