package Dominio;

import java.util.Date;

public class Unidade {
    private int cod_unidade;
    private String unidade;
    
    public Unidade(String unidade){
        this.unidade=unidade;
    }
    public int getCod_unidade() {
        return cod_unidade;
    }
    public String getUnidade() {
        return unidade;
    }
    public void setCod_Unidade(int cod_unidade) {
        this.cod_unidade = cod_unidade;
    }   
    public String toString(){
        return unidade;
    }
}
