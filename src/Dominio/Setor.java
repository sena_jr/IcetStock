package Dominio;

public class Setor extends Solicitante{
    private int codigo;
    private String nome;
    private Individual responsavel;
    
    public Setor(int codigo,String nome,Individual responsavel){
        this.codigo=codigo;
        this.nome=nome;
        this.responsavel=responsavel;
    }
    public int getCodigo() {
        return codigo;
    }
     public void setCodigo(int cod) {
        this.codigo=cod;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Individual getResponsavel() {
        return responsavel;
    }
    public String toString(){
        return nome;
    }

    
    
}
