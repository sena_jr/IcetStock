package Dominio;

public class ItemMaterial {
    private int codigo_barra;
    private Material tipoMaterial;
    private Entrada entrada;
    private int quantidade;
    
    public ItemMaterial(int codigo, Material material, Entrada entrada, int quantidade){
        this.codigo_barra = codigo;
        this.tipoMaterial = material;
        this.entrada = entrada;
        this.quantidade = quantidade;
    }
    public int getCodigo_barra() {
        return codigo_barra;
    }
    public void setCodigo_barra(int codigo_barra){
        this.codigo_barra = codigo_barra;
    }
    public Material getTipoMaterial() {
        return tipoMaterial;
    }
    public Entrada getEntrada() {
        return entrada;
    }
    public int getQuantidade(){
        return quantidade;
    }
    public String toString(){
        return ""+codigo_barra;
    }
}
