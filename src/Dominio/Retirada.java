package Dominio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Retirada {
    private int codigo;
    private Calendar data_saida;
    private Solicitante solicitante;
    private List<ItemRetirada> material = new ArrayList<ItemRetirada>();
    
    public Retirada( Calendar data, Solicitante solicitante, List<ItemRetirada> material){
        this.data_saida=data;
        this.solicitante= solicitante;
        this.material = material;
    }
    public int getCodigo() {
        return codigo;
    }
    public int setCodigo(int codigo) {
        return codigo;
    }
    public Calendar getData_saida() {
        return data_saida;
    }
    public Solicitante getSolicitante() {
        return solicitante;
    }
    public List<ItemRetirada> getMaterial(){
        return material;
    }
}
