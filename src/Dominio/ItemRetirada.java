package Dominio;

public class ItemRetirada {

    private int cod;
    private ItemMaterial itemMaterial;
    private int quantidade;
    
    public ItemRetirada(ItemMaterial item, int quantidade){
        this.itemMaterial = item;
        this.quantidade = quantidade;
    }
    
    
    public int getCod() {
        return cod;
    } 
    public ItemMaterial getItemMaterial() {
        return itemMaterial;
    }   
    public int getQuantidade() {
        return quantidade;
    }
    public void setCod(int cod){
        this.cod = cod;
    }
    public String toString(){
        return itemMaterial.getCodigo_barra()+"";
    }
   
}
