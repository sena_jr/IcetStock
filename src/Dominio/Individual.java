package Dominio;

public class Individual extends Solicitante{
    private int SIAPE;
    private String nome;
    
    public Individual(int codigo, String nome){
        this.SIAPE = codigo;
        this.nome = nome;
    }
    public int getCodigo() {
        return SIAPE;
    }
    public String getNome() {
        return nome;
    }  
    public String toString(){
        return nome;
    }

    public void setNome(String nomeNovo) {
       this.nome = nome;
    }
}
