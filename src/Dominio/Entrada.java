package Dominio;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Entrada {
    private int codigo_entrada;
    private Calendar data_entrada;
    private Aquisicao tipo_aquisicao;
    private Fornecedor fornecedor;
   

    public Entrada( Calendar data_entrada, Aquisicao tipo_aquisicao, Fornecedor fornecedor){
        this.data_entrada = data_entrada;
        this.tipo_aquisicao = tipo_aquisicao;
        this.fornecedor = fornecedor;
        
    }
    public int getCodigo_entrada() {
        return codigo_entrada;
    }
    public Calendar getData_entrada() {
        return data_entrada;
    }
    public Aquisicao getTipo_aquisicao() {
        return tipo_aquisicao;
    }
    public Fornecedor getFornecedor() {
        return fornecedor;
    }
   
    public void setCodigo_entrada(int cod){
        this.codigo_entrada=cod;
    }

    
    
}
