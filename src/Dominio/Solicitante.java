package Dominio;

public class Solicitante {
    private int cod_solicitante;
    private String email;
    
    public void setCodigoSolicitante(int cod){
        this.cod_solicitante = cod;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public int getCod_solicitante() {
        return cod_solicitante;
    }
    public String getEmail() {
        return email;
    } 
}
