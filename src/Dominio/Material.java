package Dominio;

public class Material {
    private int codigo;
    private int quantidade;
    private String descricao;
    private Unidade tipo_unidade;
    
    public Material(int quantidade,String descricao, Unidade tipo_unidade){
        this.quantidade = quantidade;
        this.descricao = descricao;
        this.tipo_unidade = tipo_unidade;
    }
    public int getCodigo() {
        return codigo;
    }
    public int getQuantidade() {
        return quantidade;
    }
    public String getDescricao() {
        return descricao;
    }
    public Unidade getTipo_unidade() {
        return tipo_unidade;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public String toString(){
        return this.descricao;
    }
}
