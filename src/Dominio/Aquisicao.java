package Dominio;

public class Aquisicao {
    private int cod_aquisicao;
    private String aquisicao;
    
    public Aquisicao(String aquisicao){
        this.aquisicao = aquisicao;
    }
    public int getCod_aquisicao() {
        return cod_aquisicao;
    }
    public void setCod_aquisicao(int cod_aquisicao) {
        this.cod_aquisicao = cod_aquisicao;
    }
    public String getAquisicao() {
        return this.aquisicao;
    }
    public String toString(){
        return aquisicao;
    }
}
