package Views;

import Dao.IndividualDAO;
import Dao.SetorDAO;
import Dominio.Individual;
import Dominio.Setor;
import Dominio.Solicitante;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class SetorCadastro extends javax.swing.JFrame {
    private Individual individual;
    private IndividualDAO individualDAO = new IndividualDAO();
    private SetorDAO setorDAO = new SetorDAO();
    private Setor setor;
            

    public SetorCadastro() {
        this.setLocationRelativeTo(null);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        branco = new javax.swing.JPanel();
        labelCodigoSetor = new javax.swing.JLabel();
        labelNomeSetor = new javax.swing.JLabel();
        labelEmailSetor = new javax.swing.JLabel();
        labelCodigoResponsavel = new javax.swing.JLabel();
        tfCodigoResponsavel = new javax.swing.JTextField();
        tfNomeSetor = new javax.swing.JTextField();
        tfEmailSetor = new javax.swing.JTextField();
        tfCodigoSetor = new javax.swing.JTextField();
        btCriar = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btUserSIAPE = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelCodigoSetor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelCodigoSetor.setText("Código do Setor:");
        branco.add(labelCodigoSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 70, 140, 60));

        labelNomeSetor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelNomeSetor.setText("Nome:");
        branco.add(labelNomeSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 120, 70, 60));

        labelEmailSetor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelEmailSetor.setText("Email:");
        branco.add(labelEmailSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 180, 50, 60));

        labelCodigoResponsavel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelCodigoResponsavel.setText("Codigo Responsavel:");
        branco.add(labelCodigoResponsavel, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 230, -1, 60));
        branco.add(tfCodigoResponsavel, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 90, 350, 30));
        branco.add(tfNomeSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 140, 350, 30));
        branco.add(tfEmailSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 190, 350, 30));
        branco.add(tfCodigoSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 240, 350, 30));

        btCriar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_add_circle_outline_black_18dp.png"))); // NOI18N
        btCriar.setText("Salvar ");
        btCriar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCriar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCriarActionPerformed(evt);
            }
        });
        branco.add(btCriar, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, 90, -1));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton17.setText("Sobre");
        jButton17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cadastro do Setor");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, 520, 70));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btCriarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCriarActionPerformed
        try{
            int codigoSolicitante = Integer.parseInt(tfCodigoResponsavel.getText().toString().trim());
            int codigoSetor = Integer.parseInt(tfCodigoSetor.getText().toString().trim());
            String nomeSetor = tfNomeSetor.getText().toString().trim();
            String emailSetor = tfEmailSetor.getText().toString().trim();
            individual = individualDAO.buscarIndividual(codigoSolicitante);
            setor = setorDAO.buscarSetor(codigoSetor);
            if(setor == null){
                if (individual != null){
                setor = new Setor(codigoSetor, nomeSetor, individual);
                setor.setEmail(emailSetor);
                setorDAO.salvarSetor(setor);
                SetorHome setorHome = new SetorHome();
                setorHome.setVisible(true);
                dispose();
                }
                else if(individual == null) {
                    JOptionPane.showMessageDialog(null, "Este funcionario nao esta cadastrado");
                }else{
                    JOptionPane.showMessageDialog(null, "Insira um codigo de funcionario valido");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Codigo de setor ja registrado");
            }
        }catch(NumberFormatException erro){
            JOptionPane.showMessageDialog(null, "Prencha os codigos corretamente");
        } catch (SQLException ex) {
            Logger.getLogger(SetorCadastro.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }//GEN-LAST:event_btCriarActionPerformed

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btCriar;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel labelCodigoResponsavel;
    private javax.swing.JLabel labelCodigoSetor;
    private javax.swing.JLabel labelEmailSetor;
    private javax.swing.JLabel labelNomeSetor;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField tfCodigoResponsavel;
    private javax.swing.JTextField tfCodigoSetor;
    private javax.swing.JTextField tfEmailSetor;
    private javax.swing.JTextField tfNomeSetor;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables
}
