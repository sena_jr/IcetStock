/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Dao.ItemMaterialDAO;
import Dao.MaterialDAO;
import Dominio.ItemMaterial;
import Dominio.Material;
import Utils.ModelTable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author User
 */
public class MaterialView extends javax.swing.JFrame {

    private List<ItemMaterial> itensMaterial = new ArrayList<ItemMaterial>();
    private ItemMaterial itemMaterial;
    private ItemMaterialDAO itemMaterialDAO = new ItemMaterialDAO();
    private ModelTable modelo;
    private Material material;
    private MaterialDAO materialDAO = new MaterialDAO();
    
    public MaterialView(Material material) {
        this.material = material;
        initComponents();
        labelTipoMaterial.setText(material.getDescricao());
        tfNovoNome.setText(material.getDescricao());
        try {
            iniciatabelaMaterial(material);
        } catch (SQLException ex) {
            Logger.getLogger(MaterialView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private MaterialView() {
       
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaItemMaterial = new javax.swing.JTable();
        verde = new javax.swing.JPanel();
        branco = new javax.swing.JPanel();
        btEdit = new javax.swing.JButton();
        tfNovoNome = new javax.swing.JTextField();
        labelTipoMaterial = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btUserSIAPE = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabelaItemMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaItemMaterial);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 260, 700, 420));

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_create_black_18dp.png"))); // NOI18N
        btEdit.setText("editar");
        btEdit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEditActionPerformed(evt);
            }
        });
        branco.add(btEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 40, 100, 40));
        branco.add(tfNovoNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 250, 40));

        labelTipoMaterial.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelTipoMaterial.setText("Tipo Material:");
        branco.add(labelTipoMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 40, 120, 50));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton17.setText("Sobre");
        jButton17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Editar de Material");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, 520, 70));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEditActionPerformed
        String novoNome = tfNovoNome.getText().toString().trim();
        if(!(novoNome.equals("") || novoNome.isEmpty())){
            material.setDescricao(novoNome);
            try {
                if(materialDAO.mudaNome(material)){
                    JOptionPane.showMessageDialog(null,"Alteração realizada");
                     MaterialHome home = new MaterialHome();
                    home.setVisible(true);
                    
                    dispose();
                }
            } catch (SQLException ex) {
                Logger.getLogger(MaterialView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else
            JOptionPane.showMessageDialog(null,"Preencha o campo corretamente");
    }//GEN-LAST:event_btEditActionPerformed

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MaterialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MaterialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MaterialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MaterialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MaterialView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btEdit;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelTipoMaterial;
    private javax.swing.JLabel logo;
    private javax.swing.JTable tabelaItemMaterial;
    private javax.swing.JTextField tfNovoNome;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

     private void iniciatabelaMaterial(Material material) throws SQLException {
       String[] colunas ={"Codigo","Data de entrada", "Quantidade"};
       ArrayList dados = new ArrayList();
       itensMaterial = itemMaterialDAO.selecionarItemMaterialByMaterial(material.getCodigo());
       for(ItemMaterial item: itensMaterial){
           Calendar cal = item.getEntrada().getData_entrada();
           String data = cal.get(Calendar.DAY_OF_MONTH)+"/"+cal.get(Calendar.MONTH)+"/"+cal.get(Calendar.YEAR);
           dados.add(new Object[]{
              item.getCodigo_barra(),
              data,
              item.getQuantidade()
           });
       }
       modelo = new ModelTable(dados, colunas);
       tabelaItemMaterial.setModel(modelo);
       tabelaItemMaterial.setAutoResizeMode(tabelaItemMaterial.AUTO_RESIZE_ALL_COLUMNS);
       tabelaItemMaterial.getSelectionModel().addListSelectionListener(selecionouLinha());
    }

    private ListSelectionListener selecionouLinha() {
         return new ListSelectionListener() {
           @Override
           public void valueChanged(ListSelectionEvent e) {
               if(tabelaItemMaterial.getSelectedRow() != -1){
                   int idItem = (int) tabelaItemMaterial.getValueAt(tabelaItemMaterial.getSelectedRow(),0);
                   try {
                       itemMaterial = itemMaterialDAO.buscarItemMaterial(idItem);
                       if(confirmaExclusao()){
                           itemMaterialDAO.deletarItemMaterial(itemMaterial);
                           tabelaItemMaterial.remove(idItem);
                            JOptionPane.showMessageDialog(null,"Item Excluido");
                       }
                   } catch (SQLException ex) {
                       Logger.getLogger(MaterialView.class.getName()).log(Level.SEVERE, null, ex);
                   }       
               }
           }  
       };
    }
    private boolean confirmaExclusao() {
        boolean tipo;// true para individual, false para setor
        Object[] options = {"Confirmar","Excluir"};
        int resposta = JOptionPane.showOptionDialog(null, "Deseja realmente excluir?", "Exclusão", JOptionPane.DEFAULT_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,null);
        if(resposta == 0)
            tipo = true;
        else 
            tipo = false;
       return tipo;
        
    }
    
}
