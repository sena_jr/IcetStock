package Views;

import Dao.SetorDAO;
import Dominio.Setor;
import Utils.ModelTable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import Views.SetorView;

public class SetorHome extends javax.swing.JFrame {

    private ModelTable modelo;
    private List<Setor> setores = new ArrayList<Setor>();
    private Setor setor;
    private SetorDAO setorDAO = new SetorDAO();
    
    public SetorHome() {
       this.setLocationRelativeTo(null);
        initComponents();
        try {
            inciarTabelaSetores();
        } catch (SQLException ex) {
            Logger.getLogger(SetorHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        logo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton17 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btUserSIAPE = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        branco = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaSetor = new javax.swing.JTable();
        menu = new javax.swing.JPanel();
        btCadastrarSetor = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton17.setText("Sobre");
        jButton17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Setores  ");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 80, 220, 70));

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabelaSetor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tabelaSetor);

        branco.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 770, 410));

        menu.setBackground(new java.awt.Color(255, 255, 255));
        menu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btCadastrarSetor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btCadastrarSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_add_circle_outline_black_18dp.png"))); // NOI18N
        btCadastrarSetor.setText("Cadastrar");
        btCadastrarSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCadastrarSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCadastrarSetorActionPerformed(evt);
            }
        });
        menu.add(btCadastrarSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 100, 40));

        branco.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, 60));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btCadastrarSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCadastrarSetorActionPerformed
        SetorCadastro setorCadastro = new SetorCadastro();
        setorCadastro.setVisible(true);
        dispose();
    }//GEN-LAST:event_btCadastrarSetorActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btCadastrarSetor;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel logo;
    private javax.swing.JPanel menu;
    private javax.swing.JTable tabelaSetor;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

    private void inciarTabelaSetores() throws SQLException {
        setores = setorDAO.selecionarSetor();
        String[] colunas = {"Codigo setor","Nome Setor", "Responsavel pelo setor"};
        ArrayList dados = new ArrayList();
        for(Setor setor:setores){
            dados.add(new Object[]{
                setor.getCodigo(),
                setor.getNome(),
                setor.getResponsavel()
            });
        }
        modelo = new ModelTable(dados, colunas);
        tabelaSetor.setModel(modelo);
        tabelaSetor.setAutoResizeMode(tabelaSetor.AUTO_RESIZE_ALL_COLUMNS);
        tabelaSetor.getSelectionModel().addListSelectionListener(selecionouLinha());
    }
    
    private ListSelectionListener selecionouLinha(){
        return new ListSelectionListener() {
           @Override
           public void valueChanged(ListSelectionEvent e) {
               if(tabelaSetor.getSelectedRow() != -1){
                   int cod_setor = (int) tabelaSetor.getValueAt(tabelaSetor.getSelectedRow(),0);
                   try {
                       Setor setor = setorDAO.buscarSetor(cod_setor);
                       SetorView setorView = new SetorView(setor);
                       setorView.setVisible(true);
                       dispose();
                       
                   } catch (SQLException ex) {
                       Logger.getLogger(MaterialHome.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   
                   
               }
           }
       };
    }
    
}
