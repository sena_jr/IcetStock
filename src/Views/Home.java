package Views;

import Dao.EntradaDAO;
import Dao.ItemMaterialDAO;
import Dao.RetiradaDAO;
import Dao.SolicitanteDAO;
import Dominio.Entrada;
import Dominio.ItemMaterial;
import Dominio.ItemRetirada;
import Dominio.Retirada;
import Utils.ModelTable;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.BackgroundImage;
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class Home extends javax.swing.JFrame {
    private List<Retirada> retiradas = new ArrayList<Retirada>();
    private List<Entrada> entradas = new ArrayList<Entrada>();
    private List<ItemMaterial> itensMaterial = new ArrayList<ItemMaterial>();
    public Home() {
        initComponents();
        setResizable(false);  
        this.setTitle("IcetStoque - Tela Principal");
        
        try {
            iniciarTabel();
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        logo = new javax.swing.JLabel();
        verde = new javax.swing.JPanel();
        branco1 = new javax.swing.JPanel();
        scrolAtividade = new javax.swing.JScrollPane();
        tableAtividade = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btUserSIAPE = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        labelHome = new javax.swing.JLabel();
        labelHome1 = new javax.swing.JLabel();
        labelHome2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        getContentPane().add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setForeground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        branco1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tableAtividade.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrolAtividade.setViewportView(tableAtividade);

        branco1.add(scrolAtividade, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 54, 790, 440));

        jLabel2.setText("Atividades Diaria:");
        branco1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        verde.add(branco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton15.setText("Sobre");
        jButton15.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("UFAM");
        jLabel1.setAlignmentX(5.0F);
        jLabel1.setAlignmentY(5.0F);
        verde.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        labelHome.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHome.setForeground(new java.awt.Color(255, 255, 255));
        labelHome.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelHome.setText("Instituto de Ciências Exatas e Tecnologia");
        verde.add(labelHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 850, 40));

        labelHome1.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        labelHome1.setForeground(new java.awt.Color(255, 255, 255));
        labelHome1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelHome1.setText("IcetStock");
        verde.add(labelHome1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 100, 770, 40));

        labelHome2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        labelHome2.setForeground(new java.awt.Color(255, 255, 255));
        labelHome2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelHome2.setText("Universidade Federal do Amazonas ");
        verde.add(labelHome2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 850, 40));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco1;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton15;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel labelHome;
    private javax.swing.JLabel labelHome1;
    private javax.swing.JLabel labelHome2;
    private javax.swing.JLabel logo;
    private javax.swing.JScrollPane scrolAtividade;
    private javax.swing.JTable tableAtividade;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

    public void iniciarTabel() throws SQLException{
        tableAtividade.removeAll();
        String[] colunas = {"Material", "Codigo","Operacao","Quantidade","Responsavel"};
        ArrayList dados = new ArrayList();
        SolicitanteDAO solicitante = new SolicitanteDAO();
        try {
            this.retiradas = new RetiradaDAO().buscarRetiradaHoje(Calendar.getInstance());
            this.entradas = new EntradaDAO().buscarRetiradaHoje(Calendar.getInstance());
            
            for(Entrada entrada:entradas){
                this.itensMaterial.addAll(new ItemMaterialDAO().selecionarItemMaterialByEntrada(entrada.getCodigo_entrada()));
            }
        } catch (ParseException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            for(ItemMaterial item:itensMaterial){
                dados.add(new Object[]{
                    item.getTipoMaterial().getDescricao(),
                    item.getCodigo_barra(),
                    "ENTRADA",
                    item.getQuantidade(),
                    item.getEntrada().getFornecedor()
            });
        }
        
        
        for(Retirada retirada:retiradas){
            for(ItemRetirada item:retirada.getMaterial()){
               dados.add(new Object[]{
                    item.getItemMaterial().getTipoMaterial().getDescricao(),
                    item.getItemMaterial().getCodigo_barra(),
                    "SAIDA",
                    item.getQuantidade(),
                    solicitante.buscarSolicitanteChildren(retirada.getSolicitante().getCod_solicitante())
                });
            }
        }
        
        
       
        
        ModelTable model = new ModelTable(dados, colunas);
        
        tableAtividade.setModel(model);
        tableAtividade.setAutoResizeMode(tableAtividade.AUTO_RESIZE_ALL_COLUMNS);
    }

}
