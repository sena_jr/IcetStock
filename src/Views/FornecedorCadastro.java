package Views;

import Dao.FornecedorDAO;
import Dominio.Fornecedor;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class FornecedorCadastro extends javax.swing.JFrame {
    private FornecedorDAO fornecedorDAO = new FornecedorDAO();
    private Fornecedor fornecedor;
    public FornecedorCadastro() {
        this.setLocationRelativeTo(null);
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        jButton15 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btUserSIAPE = new javax.swing.JButton();
        labelHome = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        branco = new javax.swing.JPanel();
        labelDescricao = new javax.swing.JLabel();
        btSalvar = new javax.swing.JButton();
        labelUnidade = new javax.swing.JLabel();
        tfNomeFornecedor = new javax.swing.JTextField();
        tfCodigoFornecedor = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setForeground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton15.setText("Sobre");
        jButton15.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.setBorderPainted(false);
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        labelHome.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        labelHome.setForeground(new java.awt.Color(255, 255, 255));
        labelHome.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelHome.setText("Cadastro do Fornecedor");
        labelHome.setAlignmentX(5.0F);
        labelHome.setAlignmentY(5.0F);
        verde.add(labelHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 70, 620, 40));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("UFAM");
        jLabel1.setAlignmentX(5.0F);
        jLabel1.setAlignmentY(5.0F);
        verde.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelDescricao.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelDescricao.setText("Nome Fornecedor:");
        branco.add(labelDescricao, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 150, 30));

        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_add_circle_outline_black_18dp.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });
        branco.add(btSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 220, 120, -1));

        labelUnidade.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelUnidade.setText("Codigo Fornecedor:");
        branco.add(labelUnidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 160, 160, 30));

        tfNomeFornecedor.setAlignmentY(0.0F);
        tfNomeFornecedor.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        branco.add(tfNomeFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 100, 470, 30));

        tfCodigoFornecedor.setAlignmentY(0.0F);
        tfCodigoFornecedor.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        branco.add(tfCodigoFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 160, 470, 30));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        String nome = tfNomeFornecedor.getText().toString();
        String codigo = tfCodigoFornecedor.getText().toString().trim();
        if (codigo.equals("") || codigo.isEmpty() || nome.isEmpty() || nome.equals("")) {
            JOptionPane.showMessageDialog(null, "Preencha os campos corretamente");

        } else {
            try {
                int codigoFornecedor = Integer.parseInt(codigo);

                fornecedor = fornecedorDAO.buscarFornecedor(codigoFornecedor);
                if(fornecedor != null)
                JOptionPane.showMessageDialog(null, "Fornecedor já Cadastrado","ERRO",JOptionPane.ERROR_MESSAGE);
                else{
                    fornecedor = new Fornecedor(codigoFornecedor, nome);
                    fornecedorDAO.salvarFornecedor(fornecedor);
                    JOptionPane.showMessageDialog(null, "Fornecedor Cadastrado");
                    FornecedorHome home = new FornecedorHome();
                    home.setVisible(true);
                    dispose();
                }

            } catch (SQLException ex) {
                Logger.getLogger(FornecedorCadastro.class.getName()).log(Level.SEVERE, null, ex);
            }catch(NumberFormatException erro){
                JOptionPane.showMessageDialog(null, "Insira os digitos do codigo do fornecedor","ERRO",JOptionPane.ERROR_MESSAGE);
            }

        }
    }//GEN-LAST:event_btSalvarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FornecedorCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FornecedorCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FornecedorCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FornecedorCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FornecedorCadastro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton15;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel labelDescricao;
    private javax.swing.JLabel labelHome;
    private javax.swing.JLabel labelUnidade;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField tfCodigoFornecedor;
    private javax.swing.JTextField tfNomeFornecedor;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables
}
