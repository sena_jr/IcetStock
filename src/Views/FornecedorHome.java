/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Dao.FornecedorDAO;
import Dominio.Fornecedor;
import Utils.ModelTable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author User
 */
public class FornecedorHome extends javax.swing.JFrame {

    private List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
    private FornecedorDAO fornecedorDAO = new FornecedorDAO();
    private Fornecedor fornecedor;
    public FornecedorHome() {
        this.setLocationRelativeTo(null);
        initComponents();
        try {
            iniciaTabelaFornecedores();
        } catch (SQLException ex) {
            Logger.getLogger(FornecedorHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jButton15 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btUserSIAPE = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        menu = new javax.swing.JPanel();
        btCadastrarFornecedor = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabelaFornecedores = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Fornecedores ");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 350, 70));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton15.setText("Sobre");
        jButton15.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        menu.setBackground(new java.awt.Color(255, 255, 255));
        menu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btCadastrarFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_add_circle_outline_black_18dp.png"))); // NOI18N
        btCadastrarFornecedor.setText("Cadastrar");
        btCadastrarFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCadastrarFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCadastrarFornecedorActionPerformed(evt);
            }
        });
        menu.add(btCadastrarFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 120, 40));

        jPanel1.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, 60));

        tabelaFornecedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tabelaFornecedores);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 750, 410));

        verde.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btCadastrarFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCadastrarFornecedorActionPerformed
        FornecedorCadastro fornecedorCadastro = new FornecedorCadastro();
        fornecedorCadastro.setVisible(true);
        dispose();
    }//GEN-LAST:event_btCadastrarFornecedorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FornecedorHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FornecedorHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FornecedorHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FornecedorHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FornecedorHome().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCadastrarFornecedor;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel logo;
    private javax.swing.JPanel menu;
    private javax.swing.JTable tabelaFornecedores;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

    private void iniciaTabelaFornecedores() throws SQLException {
       fornecedores = fornecedorDAO.selecionarFornecedor();
       String[] colunas = {"Codigo do Fornecedor","Nome Fornecedor"};
       ArrayList dados = new ArrayList();
       for(Fornecedor fornecedor:fornecedores){
           dados.add(new Object[]{
               fornecedor.getCod_fornecedor(),
               fornecedor.getNome_fornecedor(),
           });
       }
       ModelTable modelo = new ModelTable(dados, colunas);
       tabelaFornecedores.setModel(modelo);
       tabelaFornecedores.setAutoResizeMode(tabelaFornecedores.AUTO_RESIZE_ALL_COLUMNS);
       
       
    }
    
}
