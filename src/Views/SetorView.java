/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Dao.SetorDAO;
import Dominio.Setor;
import Dominio.Solicitante;
import Utils.ModelTable;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author marce
 */
public class SetorView extends javax.swing.JFrame {
    
    private SetorDAO setorDAO = new SetorDAO();
    private Setor setor;
    
    
    public SetorView(Setor setor) {
        this.setor = setor;
        initComponents();
        tfNovoEmail.setText(setor.getEmail());
        tfNovoNome.setText(setor.getNome());
    }
    
    private SetorView(){
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        branco = new javax.swing.JPanel();
        tfNovoEmail = new javax.swing.JTextField();
        tfNovoNome = new javax.swing.JTextField();
        labelEmail = new javax.swing.JLabel();
        labelNomeSetor = new javax.swing.JLabel();
        btCancelar = new javax.swing.JButton();
        btSalvar = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        logo = new javax.swing.JLabel();
        btRelatorio = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btUserSIAPE = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        branco.add(tfNovoEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 170, 280, 30));

        tfNovoNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfNovoNomeActionPerformed(evt);
            }
        });
        branco.add(tfNovoNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 120, 280, 30));

        labelEmail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelEmail.setText("Email:");
        branco.add(labelEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 180, -1, -1));

        labelNomeSetor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelNomeSetor.setText("Nome:");
        branco.add(labelNomeSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, -1, -1));

        btCancelar.setText("Cancelar");
        btCancelar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });
        branco.add(btCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 230, 90, 40));

        btSalvar.setText("Salvar");
        btSalvar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });
        branco.add(btSalvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 230, 80, 40));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton17.setText("Sobre");
        jButton17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Editar do Setor");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 80, 520, 70));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tfNovoNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfNovoNomeActionPerformed

    }//GEN-LAST:event_tfNovoNomeActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        String novoNome = tfNovoNome.getText().toString().trim();
        String novoEmail = tfNovoEmail.getText().toString().trim();
        if(!(novoNome.equals("") || novoNome.isEmpty()||novoEmail.equals("")||novoEmail.isEmpty())){
            setor.setNome(novoNome);
            setor.setEmail(novoEmail);
            try {
                if(setorDAO.mudaNome(setor)){
                    JOptionPane.showMessageDialog(null,"Alteração realizada");
                    SetorHome home = new SetorHome();
                    home.setVisible(true);
                    dispose();
                    
                } else {
                }
            } catch (SQLException ex) {
                Logger.getLogger(SetorView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else
            JOptionPane.showMessageDialog(null,"Preencha o campo corretamente");
    }//GEN-LAST:event_btSalvarActionPerformed
    private void initComponentes (){
        
        
        tfNovoNome = new javax.swing.JTextField();
        tfNovoEmail = new javax.swing.JTextField();
        labelNomeSetor = new javax.swing.JLabel();
        labelEmail = new  javax.swing.JLabel();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        
        labelNomeSetor.setText("Nome: ");
        getContentPane().add(labelNomeSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 90, 50));
        
        
    }
    
    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {                                         
       SetorHome home = new SetorHome();
       home.setVisible(true);
       dispose();
    }
   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SetorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SetorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SetorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SetorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SetorView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel labelEmail;
    private javax.swing.JLabel labelNomeSetor;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField tfNovoEmail;
    private javax.swing.JTextField tfNovoNome;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

}
