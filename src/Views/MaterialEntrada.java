package Views;

import Dao.AquisicaoDAO;
import Dao.EntradaDAO;
import Dao.FornecedorDAO;
import Dao.ItemMaterialDAO;
import Dao.MaterialDAO;
import Dominio.Aquisicao;
import Dominio.Entrada;
import Dominio.Fornecedor;
import Dominio.ItemMaterial;
import Dominio.Material;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marce
 */
public class MaterialEntrada extends javax.swing.JFrame {
    private DefaultListModel modelo= new DefaultListModel();
    private List<Material> materiais = new ArrayList<Material>();
    private List<ItemMaterial> itensMaterial = new ArrayList<ItemMaterial>();
    private List<Aquisicao> aquisicoes = new ArrayList<Aquisicao>();
    private ItemMaterial itemMaterial;
    private ItemMaterialDAO itemMaterialDAO = new ItemMaterialDAO();
    private MaterialDAO materialDAO = new MaterialDAO();
    private Fornecedor fornecedor;
    private AquisicaoDAO aquisicaoDAO = new AquisicaoDAO();
    private Aquisicao aquisicao;
    private FornecedorDAO fornecedorDAO = new FornecedorDAO();
    private EntradaDAO  entradaDAO = new EntradaDAO();
    private Entrada entrada;
    
    public MaterialEntrada() {
        this.setLocationRelativeTo(null);
        initComponents();
        String data = Calendar.getInstance().toString();
        listaEntrada.setModel(modelo);
        
        try {
            iniciarComboMateriais();
            inciatComboAquisicao();
        } catch (SQLException ex) {
            Logger.getLogger(MaterialEntrada.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        verde = new javax.swing.JPanel();
        jButton17 = new javax.swing.JButton();
        btRelatorio = new javax.swing.JButton();
        btSetor = new javax.swing.JButton();
        btFornecedor = new javax.swing.JButton();
        btMaterial = new javax.swing.JButton();
        btUserSIAPE = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        branco = new javax.swing.JPanel();
        labelMaterial = new javax.swing.JLabel();
        labelMaterial1 = new javax.swing.JLabel();
        labelAquisicao = new javax.swing.JLabel();
        labelCodigo = new javax.swing.JLabel();
        labelQuantidade = new javax.swing.JLabel();
        labelData = new javax.swing.JLabel();
        comboBoxMaterial = new javax.swing.JComboBox<Material>();
        tfFornecedor = new javax.swing.JTextField();
        comboBoxAquisicao = new javax.swing.JComboBox<Aquisicao>();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaEntrada = new javax.swing.JList<ItemMaterial>();
        btConcluirEntrada = new javax.swing.JButton();
        labelListaVazia = new javax.swing.JLabel();
        tfCodigo = new javax.swing.JTextField();
        tfQuantidade = new javax.swing.JTextField();
        ftfData = new javax.swing.JFormattedTextField();
        btAdicionarItem = new javax.swing.JButton();
        logo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        verde.setBackground(new java.awt.Color(0, 51, 51));
        verde.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_info_black_18dp.png"))); // NOI18N
        jButton17.setText("Sobre");
        jButton17.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        verde.add(jButton17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 610, 130, 60));

        btRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_file_copy_black_18dp.png"))); // NOI18N
        btRelatorio.setText("Relatório");
        btRelatorio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRelatorioActionPerformed(evt);
            }
        });
        verde.add(btRelatorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 130, 60));

        btSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_home_black_18dp.png"))); // NOI18N
        btSetor.setText("Setor");
        btSetor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSetorActionPerformed(evt);
            }
        });
        verde.add(btSetor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 130, 60));

        btFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sharp_local_shipping_black_18dp.png"))); // NOI18N
        btFornecedor.setText("Fornecedor ");
        btFornecedor.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFornecedorActionPerformed(evt);
            }
        });
        verde.add(btFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 130, 60));

        btMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_unarchive_black_18dp.png"))); // NOI18N
        btMaterial.setText("Material");
        btMaterial.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMaterialActionPerformed(evt);
            }
        });
        verde.add(btMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 130, 60));

        btUserSIAPE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_person_black_18dp.png"))); // NOI18N
        btUserSIAPE.setText("Funcionário");
        btUserSIAPE.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btUserSIAPE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUserSIAPEActionPerformed(evt);
            }
        });
        verde.add(btUserSIAPE, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 130, 60));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Entrada de Material");
        jLabel2.setAlignmentX(5.0F);
        jLabel2.setAlignmentY(5.0F);
        verde.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 80, 520, 70));

        branco.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelMaterial.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelMaterial.setText("Tipo Material:");
        branco.add(labelMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, -1, 60));

        labelMaterial1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelMaterial1.setText("Fornecedor:");
        branco.add(labelMaterial1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 140, -1, 60));

        labelAquisicao.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelAquisicao.setText("Tipo de Aquisição:");
        branco.add(labelAquisicao, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, 60));

        labelCodigo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelCodigo.setText("Codigo Material:");
        branco.add(labelCodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, -1, 60));

        labelQuantidade.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelQuantidade.setText("Quantidade:");
        branco.add(labelQuantidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 300, -1, 60));

        labelData.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelData.setText("Data entrada:");
        branco.add(labelData, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, -1, 60));

        branco.add(comboBoxMaterial, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 110, 270, 30));

        tfFornecedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfFornecedorKeyPressed(evt);
            }
        });
        branco.add(tfFornecedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 270, 30));

        branco.add(comboBoxAquisicao, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 270, 30));

        listaEntrada.setEnabled(false);
        jScrollPane1.setViewportView(listaEntrada);

        branco.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 80, 350, 390));

        btConcluirEntrada.setText("Salvar Entrada");
        btConcluirEntrada.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btConcluirEntrada.setEnabled(false);
        btConcluirEntrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btConcluirEntradaActionPerformed(evt);
            }
        });
        branco.add(btConcluirEntrada, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 500, 110, -1));

        labelListaVazia.setText("Lista Vazia:");
        branco.add(labelListaVazia, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 60, 60, 20));

        tfCodigo.setEnabled(false);
        tfCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfCodigoKeyPressed(evt);
            }
        });
        branco.add(tfCodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 260, 200, 30));

        tfQuantidade.setEnabled(false);
        tfQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfQuantidadeActionPerformed(evt);
            }
        });
        branco.add(tfQuantidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 50, 30));

        ftfData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        branco.add(ftfData, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 360, 240, 30));

        btAdicionarItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/baseline_add_circle_outline_black_18dp.png"))); // NOI18N
        btAdicionarItem.setText("Adicionar Item");
        btAdicionarItem.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btAdicionarItem.setEnabled(false);
        btAdicionarItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAdicionarItemActionPerformed(evt);
            }
        });
        branco.add(btAdicionarItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 440, -1, -1));

        verde.add(branco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 840, 530));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo_ufam.png"))); // NOI18N
        verde.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("UFAM");
        jLabel3.setAlignmentX(5.0F);
        jLabel3.setAlignmentY(5.0F);
        verde.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 170, -1, -1));

        getContentPane().add(verde, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1000, 700));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btUserSIAPEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUserSIAPEActionPerformed
        UserSiapeHome home = new UserSiapeHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btUserSIAPEActionPerformed

    private void btMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMaterialActionPerformed
        MaterialHome home = new MaterialHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btMaterialActionPerformed

    private void btFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFornecedorActionPerformed
        FornecedorHome home = new FornecedorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btFornecedorActionPerformed

    private void btSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSetorActionPerformed
        SetorHome home = new SetorHome();
        home.setVisible(true);
        dispose();
    }//GEN-LAST:event_btSetorActionPerformed

    private void btRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRelatorioActionPerformed
        Relatorio relatorio = new Relatorio();
        relatorio.setVisible(true);
        dispose();
    }//GEN-LAST:event_btRelatorioActionPerformed

    private void btAdicionarItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAdicionarItemActionPerformed
        //itensMaterial = (List<ItemMaterial>) listaEntrada.getModel();
        String qtd = tfQuantidade.getText().toString().trim();
        String codigoMaterial = tfCodigo.getText().toString().trim();
        String data = ftfData.getText().toString().trim();
        Material material = (Material) comboBoxMaterial.getSelectedItem();
        Calendar calendar = Calendar.getInstance();
        aquisicao = (Aquisicao) comboBoxAquisicao.getSelectedItem();
        entrada = new Entrada(calendar, aquisicao, fornecedor);
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            calendar.setTime(sdf.parse(data));
        } catch (ParseException ex) {
            Logger.getLogger(MaterialEntrada.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int quantidade = Integer.parseInt(qtd);
        if(qtd.equals("") || qtd.isEmpty() || codigoMaterial.equals("") || codigoMaterial.isEmpty() ||
                data.equals("") || data.isEmpty()){
             JOptionPane.showMessageDialog(null, "Por favor, preencha os campos corretamente");
        }else if(quantidade>0){
            int codigo = Integer.parseInt(codigoMaterial);
            ItemMaterial novoItem = new ItemMaterial(codigo, material, entrada, quantidade);
            if(validaNovoItem(novoItem, itensMaterial)){
                itensMaterial.add(novoItem);
                listaEntrada.removeAll();
                modelo = new DefaultListModel();
                for(ItemMaterial item: itensMaterial){
                    modelo.addElement(item);
                }
                listaEntrada.setModel(modelo);
                itemMaterial = null;
                labelListaVazia.setVisible(false);
                btConcluirEntrada.setEnabled(true);
                tfFornecedor.setEnabled(false);
                comboBoxAquisicao.setEnabled(false);
            }else{
                JOptionPane.showMessageDialog(null, "Material Referente a este codigo ja esta na lista");
            }
        }

    }//GEN-LAST:event_btAdicionarItemActionPerformed

    private void btConcluirEntradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btConcluirEntradaActionPerformed
        try {
            int idEntrada = entradaDAO.salvarEntrada(entrada);
            for(ItemMaterial item:itensMaterial){
                item.getEntrada().setCodigo_entrada(idEntrada);
                itemMaterialDAO.salvarItemMaterial(item);
                materialDAO.entradaNoEstoque(item.getTipoMaterial(), item.getQuantidade());
                
            }
            JOptionPane.showMessageDialog(null, "Entrada cadastrada com sucesso");
            MaterialHome home = new MaterialHome();
            home.setVisible(true);
            dispose();
            
        } catch (SQLException ex) {
            Logger.getLogger(MaterialEntrada.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Algo de errado aconteceu");
        }
    }//GEN-LAST:event_btConcluirEntradaActionPerformed

    private void tfFornecedorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfFornecedorKeyPressed
        if(evt.getExtendedKeyCode() == KeyEvent.VK_ENTER){
            String cod = tfFornecedor.getText().toString().trim();
            if(cod.equals("") || cod.isEmpty()){
                JOptionPane.showMessageDialog(null, "Preencha o campo corretamente");
            }
            else{
                int codigo = Integer.parseInt(cod);
                try {
                    fornecedor = fornecedorDAO.buscarFornecedor(codigo);
                    if(fornecedor == null)
                        JOptionPane.showMessageDialog(null, "Fornecedor nao encontrado");
                    else{
                        tfCodigo.setEnabled(true);
                        btAdicionarItem.setEnabled(true);
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(MaterialEntrada.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_tfFornecedorKeyPressed

    private void tfCodigoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfCodigoKeyPressed
        if(evt.getExtendedKeyCode() == KeyEvent.VK_ENTER){
            String cod = tfCodigo.getText().toString().trim();
            if(cod.equals("") || cod.isEmpty()){
                JOptionPane.showMessageDialog(null, "Preencha o campo corretamente");
            }
            else{
                try {
                     int codigo = Integer.parseInt(cod);
                    itemMaterial = itemMaterialDAO.buscarItemMaterial(codigo);
                    if(itemMaterial != null)
                        JOptionPane.showMessageDialog(null, "Produto ja em estoque");
                    else{
                        tfQuantidade.setEnabled(true);
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(MaterialEntrada.class.getName()).log(Level.SEVERE, null, ex);
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(null, "Insira o codigo numerico do produto");
                }
            }
        }
    }//GEN-LAST:event_tfCodigoKeyPressed

    private void tfQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfQuantidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfQuantidadeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel branco;
    private javax.swing.JButton btAdicionarItem;
    private javax.swing.JButton btConcluirEntrada;
    private javax.swing.JButton btFornecedor;
    private javax.swing.JButton btMaterial;
    private javax.swing.JButton btRelatorio;
    private javax.swing.JButton btSetor;
    private javax.swing.JButton btUserSIAPE;
    private javax.swing.JComboBox<Aquisicao> comboBoxAquisicao;
    private javax.swing.JComboBox<Material> comboBoxMaterial;
    private javax.swing.JFormattedTextField ftfData;
    private javax.swing.JButton jButton17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelAquisicao;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JLabel labelData;
    private javax.swing.JLabel labelListaVazia;
    private javax.swing.JLabel labelMaterial;
    private javax.swing.JLabel labelMaterial1;
    private javax.swing.JLabel labelQuantidade;
    private javax.swing.JList<ItemMaterial> listaEntrada;
    private javax.swing.JLabel logo;
    private javax.swing.JTextField tfCodigo;
    private javax.swing.JTextField tfFornecedor;
    private javax.swing.JTextField tfQuantidade;
    private javax.swing.JPanel verde;
    // End of variables declaration//GEN-END:variables

    private void iniciarComboMateriais() throws SQLException {
       materiais = materialDAO.selecionarMaterial();
       comboBoxMaterial.removeAllItems();
       for(Material m:materiais){
           comboBoxMaterial.addItem(m);
       }
       
    }

    private void inciatComboAquisicao() throws SQLException {
        aquisicoes = aquisicaoDAO.selecionarAquisicao();
        comboBoxAquisicao.removeAllItems();
        for(Aquisicao aq:aquisicoes){
            comboBoxAquisicao.addItem(aq);
        }
    }
    private boolean validaNovoItem(ItemMaterial item,List<ItemMaterial> itens){
        for(ItemMaterial i:itens){
            if(i.getCodigo_barra() == item.getCodigo_barra())
                return false;
        }
        return true;
    }
}
